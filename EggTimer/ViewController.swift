//
//  ViewController.swift
//  EggTimer
//
//  Created by Angela Yu on 08/07/2019.
//  Copyright © 2019 The App Brewery. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    
   var player: AVAudioPlayer?
    @IBOutlet weak var progressBar: UIProgressView!
    let eggtimes=[
        "Soft":3,
        "Medium":4,
        "Hard":7
    ]
    
    var secondpassed=0
    var totaltime=0
    var timer=Timer()

    @IBOutlet weak var titleLabel: UILabel!
    @IBAction func EggAction(_ sender: UIButton) {
        timer.invalidate()
        player?.stop()
        let hardness=sender.currentTitle!
        let result=eggtimes[hardness]!
        progressBar.progress=0.0
        secondpassed=0
        titleLabel.text=hardness
        totaltime = result
        
        
        timer=Timer.scheduledTimer(timeInterval: 1.0,target: self,selector: #selector(updateTimer),userInfo: nil, repeats: true)
}
    
    @objc func updateTimer(){
        if secondpassed < totaltime{
            secondpassed += 1
            progressBar.progress = Float(secondpassed) / Float( totaltime)
            
            print(Float(secondpassed) / Float( totaltime))
        }else{
            
            timer.invalidate()
            titleLabel.text="DONE!!"
            playSound()
        }
    }
    
    
    func playSound() {
        guard let url = Bundle.main.url(forResource: "alarm_sound", withExtension: "mp3") else { return }

        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
            try AVAudioSession.sharedInstance().setActive(true)

            /* The following line is required for the player to work on iOS 11. Change the file type accordingly*/
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)

            /* iOS 10 and earlier require the following line:
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileTypeMPEGLayer3) */

            guard let player = player else { return }

            player.play()

        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    
    
    
    
    
    
    
    
    
}


//        switch hardness{
//        case    "Soft"   : print(soft)
//        case    "Medium" : print(medium)
//        case    "Hard"   : print(hard)
//        default :print("ok good")
//        }
//
        
